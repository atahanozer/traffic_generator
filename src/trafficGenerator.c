/*
 ============================================================================
 Name        : TRAFFIC_GENERATOR.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "common.h"

#define NUMBER_OF_GENERATORS MAX_NUMBER_OF_SLAVE_SERVERS // should be equal to NUMBER_OF_GENERATORS

slaveServer SLAVE_SERVERS [MAX_NUMBER_OF_SLAVE_SERVERS];

pthread_t THREADS [NUMBER_OF_GENERATORS] = {0};

int tcpSocketToMasterServer = -1;

__thread int tcpSocketToSlaveServer = -1;

void destroyMasterBridgeSocket () {
	destroySocket (tcpSocketToMasterServer);
}

void connectToMasterBridge (char * serverIp) {
	struct sockaddr_in serverAddrDesc;
	in_addr_t addr;
	int rc;

	addr = inet_addr (serverIp);

	if (addr == INADDR_NONE) {
		printf ("connectToMasterBridge - invalid ip address : %s\n", serverIp);
		exit (0);
	}

	memset (&serverAddrDesc, 0, sizeof (serverAddrDesc));
	serverAddrDesc.sin_family      = AF_INET;
	serverAddrDesc.sin_addr.s_addr = addr;
	serverAddrDesc.sin_port        = htons(TCP_MASTER_BRIDGE_PORT);

	tcpSocketToMasterServer = socket (AF_INET, SOCK_STREAM, 0);

	if (tcpSocketToMasterServer < 0) {
		printf ("connectToMasterBridge - socket failed : %s\n", strerror (errno));
		exit (0);
	}

	rc = connect (tcpSocketToMasterServer, (struct sockaddr *) &serverAddrDesc, sizeof (serverAddrDesc));

	if (rc < 0) {
		printf ("connectToMasterBridge - connect failed : %s\n", strerror (errno));
		destroyMasterBridgeSocket ();
		exit (0);
	}
}

void connectToSlaveServer (slaveServer * args) {
	struct sockaddr_in serverAddrDesc;
	int rc;

	memset (&serverAddrDesc, 0, sizeof (serverAddrDesc));
	serverAddrDesc.sin_family      = AF_INET;
	serverAddrDesc.sin_addr.s_addr = args->ipAddrInt;
	serverAddrDesc.sin_port        = htons (args->port);

	tcpSocketToSlaveServer = socket (AF_INET, SOCK_STREAM, 0);
	if (tcpSocketToSlaveServer < 0) {
		printf ("connectToSlaveServer - socket failed : %s\n", strerror (errno));
		exit (0);
	}

	rc = connect (tcpSocketToSlaveServer, (struct sockaddr *) &serverAddrDesc, sizeof (serverAddrDesc));
	if (rc < 0) {
		printf ("connectToSlaveServer - connect failed : %s\n", strerror (errno));
		exit (0);
	}
}

void buildRandomTransaction (transaction_t * transaction) {
	transaction->srcCustomerId  = 0;
	transaction->srcBranchId    = 0;
	transaction->destCustomerId = 1;
	transaction->destBranchId   = 1;
	transaction->operation      = 3;
}

void * trafficGeneratorThreadMain (void * ARGS) {
	int rc;
	int numberOfTransactions = 0;
	struct in_addr addr;
	transaction_t transaction;
	slaveServer * args = (slaveServer *) ARGS;

	addr.s_addr = args->ipAddrInt;

	connectToSlaveServer (args);

	/*
	TRANSACTIONS SHOULD BE SENT TO SLAVE SERVERS.
	*/

	while (numberOfTransactions < MAX_NUMBER_OF_TRANSACTIONS_PER_TRAFFIC_GENERATOR) {
		buildRandomTransaction (&transaction);
		rc = send (tcpSocketToSlaveServer, &transaction, sizeof (transaction), 0);
		if (rc < 0) {
			printf ("Connection to server (%s - %d) has been lost!\n", inet_ntoa(addr), args->port);
			destroySocket (tcpSocketToSlaveServer);
			return NULL;
		}

		numberOfTransactions ++;
	}

	transaction.operation = -1;

	rc = send (tcpSocketToSlaveServer, &transaction, sizeof (transaction), 0);
	if (rc < 0) {
		printf ("Connection to server (%s - %d) has been lost!\n", inet_ntoa(addr), args->port);
	}

	destroySocket (tcpSocketToSlaveServer);

	return NULL;
}

int main(int argc, char * argv []) {
	int i;
	struct in_addr addr;
	int rc;

	if (argc != 2) {
		printf ("Usage : %s <ip_address_of_master_server>\n", argv [0]);
		exit (0);
	}

	connectToMasterBridge (argv [1]);

	recvn (tcpSocketToMasterServer, &SLAVE_SERVERS, sizeof (SLAVE_SERVERS), sizeof (SLAVE_SERVERS));

	destroyMasterBridgeSocket ();

	for (i = 0 ; i < MAX_NUMBER_OF_SLAVE_SERVERS ; i++) {
		if (SLAVE_SERVERS[i].ipAddrInt != 0) {
			addr.s_addr = SLAVE_SERVERS[i].ipAddrInt;
			printf ("---------------------------------------\n");
			printf ("SLAVE_SERVERS[%d].ipAddrStr : %s\n", i, inet_ntoa(addr));
			printf ("SLAVE_SERVERS[%d].port      : %d\n", i, SLAVE_SERVERS[i].port);
			printf ("---------------------------------------\n");
			rc = pthread_create (&THREADS[i], NULL, trafficGeneratorThreadMain, (void *)&SLAVE_SERVERS[i]);
			if (rc < 0) {
				printf ("main - pthread_create failed : %s", strerror (errno));
				exit (1);
			}
		}
	}

	for (i = 0 ; i < MAX_NUMBER_OF_SLAVE_SERVERS ; i++) {
		if (THREADS[i] != 0) {
			pthread_join (THREADS[i], NULL);
		}
	}

	return 0;
}
