#ifndef __DISTRIBUTED_COMMON_H
#define __DISTRIBUTED_COMMON_H

#include <stdio.h>
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#define UDP_MASTER_LISTENER_PORT    54320
#define TCP_MASTER_BRIDGE_PORT      54321
#define UDP_SLAVE_SERVER_BASE_PORT  54322
#define TCP_SLAVE_SERVER_BASE_PORT  54353

#define MAX_TCP_CLIENTS             1

#define UDP_MASTER_TIMEOUT          250000
#define UDP_CLIENT_PERIOD           200000

#define IO_BUFFER_SIZE              64

#define MAX_NUMBER_OF_SLAVE_SERVERS 4

#define MAX_NUMBER_OF_TRANSACTIONS_PER_TRAFFIC_GENERATOR 8 //16384

#define TRUE  1
#define FALSE 0

#define diffUsec(A,B)  ((B.tv_nsec-A.tv_nsec)/1000+(B.tv_sec-A.tv_sec)*1000000)
#define getCurrentTime(A) clock_gettime(CLOCK_REALTIME,&A);

#define KEEP_ALIVE_STR    "ALIVE"
#define KEEP_ALIVE_STRLEN 5
#define CONT_STR          "CONT"
#define STOP_STR          "STOP"
#define CONT_STOP_STRLEN  4

typedef struct timespec timevar;

typedef struct slaveServer_s {
	unsigned int   ipAddrInt;
	unsigned short port;
	timevar        lastMsgTime;
	int            udpResponseFilDes;
	unsigned short udpPort;
} slaveServer;

typedef struct transaction_s {
	unsigned int   srcCustomerId;
	unsigned short srcBranchId;
	unsigned int   destCustomerId;
	unsigned short destBranchId;
	char  operation;
} transaction_t;

void recvn (int sockfd, void * buffer, int bufSize, int expectedDataSize);
void destroySocket (int sockfd);
#endif
